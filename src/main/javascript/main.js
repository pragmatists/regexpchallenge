require.config({
	baseUrl : "/scripts",
	paths : {
		underscore : ['../lib/underscore-min', 'http://underscorejs.org/underscore-min'],
		jquery : ['../lib/jquery-1.10.1.min', 'http://code.jquery.com/jquery-1.10.1.min']
	},
	shim : {
		underscore : {
			exports : "_"
		},
		jquery : {
			exports : "$"
		}
	}
});

require(["level", "level-ui", "example", "jquery"], function(Level, ui, Example, $) {

	var level = new Level({
		examples: [
		           new Example("anonymous@(yahoo.com)"),
		           new Example("john.doe@(gmail.com)"), 
		           new Example("123@invalid.com"),
		           new Example("notAnEmail@abc/de")
		          ]
	});
	
	$("body").append($(ui(level)));

});
