define(["underscore"], function(_) {
	
	return function(options){

		var _examples = [];
		var examples = function(){ return _examples};
		
		if(options && options.examples){
			_examples = options.examples;
		}
		
		return {
			examples: examples,
			solveWith: function(solution){
				var solved = _.reduce(_examples, function(levelValid, example){		
					var exampleValid = example.match(solution);
					return levelValid && exampleValid; 					
				}, true);
				return { solved: solved };
			}
		};
	}
});
