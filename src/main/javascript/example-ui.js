define(["underscore", "jquery"], function(_, $) {

	return function(example) {

		var div = $("<div class='example'/>");

		var inGroup = false;

		_.each(example.content.split(""), function(e) {

			if (e === "(") {
				inGroup = true;
				return;
			}
			if (e === ")") {
				inGroup = false;
				return;
			}

			div.append($("<span" + (inGroup ? " class='group'" : "") + ">" + e
					+ "</span>"));
		});

		div.get(0).markValid = function(){
			if(!example.isValid()){
				div.addClass("invalid");
			} else{
				div.removeClass("invalid");
			}
		};
		
		return div;

	}
});
