define(["jquery", "underscore", "example-ui"], function($, _, ui) {
	
	return function(level){

		var div = $("<div/>");
		var check = $("<a href='#'>Check</a>");
		var solutionInput = $("<input type='text'/>"); 
		
		_.each(level.examples(), function(example){
			div.append(ui(example));
		});
		
		var buttons = $("<div class='buttons'/>");
		buttons.append("Solution: ");
		buttons.append(solutionInput);
		buttons.append(check);
		div.append(buttons);
		
		check.click(function(){
			level.solveWith(solutionInput.val());
			$.each(div.find("div.example"), function(){
				console.log(this);
				this.markValid();
			});
		});

		return div; 
	}
});
