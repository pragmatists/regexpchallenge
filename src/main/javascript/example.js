define(["underscore", "jquery"], function(_, $) {

	return function(content){
	
		var matchAll = function(pattern){
			return new RegExp("^" + pattern + "$", "");
		};
		
		var replaceGroups = function(content){
			return content.replace(/[()]/g, "");
		}
		
		var expectedMatches = function(content){
			return replaceGroups(content).match(matchAll(content));
		};
		
		var valid = true;
		
		return {
			content: content,
			isValid: function(){
				return valid;
			},
			match: function(pattern){
				var regexp = matchAll(pattern);
				var result = replaceGroups(content).match(regexp);
				
				valid = _.isEqual(expectedMatches(content), result);
				return valid;
			}
		};
		
		
	}
});
