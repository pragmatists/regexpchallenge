define(["level", "example"], function(Level, Example) {

	describe("Level", function() {

		it("has no examples by default", function() {

			// given:
			var level = new Level();
			
			// when:
			var examples = level.examples();
			
			// then:
			expect(examples).toEqual([]);
		});
		
		it("has examples passed in constructor", function() {

			// given:
			var level = new Level({
				examples: ["example 1", "example 2"]
			});

			// when:
			var examples = level.examples();

			// then:
			expect(examples).toEqual(["example 1", "example 2"]);
		});
		
		it("can be solved", function() {

			// given:
			var level = new Level({
				examples: [new Example("WI (345)")]
			});
			
			// when:
			var result = level.solveWith("[A-Z]+ (\\d+)[A-Z]*");
			
			// then:
			expect(result.solved).toBeTruthy();
		});
		
		it("is not solved if Example is not solved", function() {
			
			// given:
			var level = new Level({
				examples: [new Example("WI (345)")]
			});
			
			// when:
			var result = level.solveWith("invalidSolution");
			
			// then:
			expect(result.solved).toBeFalsy();
		});
		
		
		it("is not solved if at least one Example is not solved", function() {
			
			// given:
			var level = new Level({
				examples: [new Example("WI (345)"), new Example("WI (29)GE")]
			});
			
			// when:
			var result = level.solveWith("WI (345)");
			
			// then:
			expect(result.solved).toBeFalsy();
		});

		it("is not solved if at least one Example is not solved in different order", function() {
			
			// given:
			var level = new Level({
				examples: [new Example("WI (29)GE"), new Example("WI (345)")]
			});
			
			// when:
			var result = level.solveWith("WI (345)");
			
			// then:
			expect(result.solved).toBeFalsy();
		});
		
		it("do not share examples with other instance", function() {
			
			// given:
			var level = new Level({
				examples: ["example 1"]
			});
			var otherLevel = new Level({
				examples: ["example 2"]
			});
			
			// when:
			
			// then:
			expect(level.examples()).toEqual(["example 1"]);
			expect(otherLevel.examples()).toEqual(["example 2"]);
		});
		
	});
});
