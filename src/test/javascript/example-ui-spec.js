define(["example", "example-ui"], function(Example, ui) {
	
	describe("Example (UI)", function() {
				
		it("displays itself as dom element", function() {

			// given:
			var example = new Example("ab(cd)ee");
			
			// when:
			var dom = ui(example);
			
			// then:
			expect(extractTextFromElements($(dom).find("span"))).toEqual(["a", "b", "c", "d", "e", "e"]);
			expect(extractTextFromElements($(dom).find("span.group"))).toEqual(["c", "d"]);
			
		});
	});
	
	function extractTextFromElements(elements){
		return elements.map(function() {
		    	return $(this).text();
			}).get();
	};
	
});
