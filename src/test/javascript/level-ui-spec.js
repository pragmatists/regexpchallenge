define(["level-ui", "level", "jquery", "example"], function(ui, Level, $, Example) {

	describe("Level (UI)", function() {
		
		it("renders as dom", function() {
			// given:
			var level = new Level({
				examples: [new Example("1"), new Example("2")]
			});
			
			// when
			var dom = ui(level);
			
			//then
			expect(extractTextFromElements($(dom).find("div.example"))).toEqual(["1", "2"]);
		});

		it("has controls to enter solution", function() {
			// given:
			var level = new Level();
			
			// when
			var dom = ui(level);
			
			//then
			expect($(dom).find("input[type='text']").size()).toEqual(1);
			expect($(dom).find("a").text()).toEqual("Check");
		});
		
		it("validates solution after check button clicked", function() {
			
			// given:
			var level = new Level();
			spyOn(level, "solveWith");
			
			// when
			var dom = ui(level);
			$(dom).find("input").val("solution");
			$(dom).find("a").click();
			
			//then
			expect(level.solveWith).toHaveBeenCalledWith("solution");
		});
		
		it("mark not satisfied examples", function() {
			
			// given:
			var level = new Level({
				examples: [new Example("abc(de)f"), new Example("123")]
			});
			
			// when
			var dom = ui(level);
			$(dom).find("input").val("...(de).");
			$(dom).find("a").click();
			
			//then
			expect($(dom).find("div.example").eq(0).hasClass("invalid")).toBeFalsy();
			expect($(dom).find("div.example").eq(1).hasClass("invalid")).toBeTruthy();
		});
		
		

		function extractTextFromElements(elements){
			return elements.map(function() {
			    	return $(this).text();
				}).get();
		};
	});
});
