define(["example"], function(Example) {
	
	describe("Example", function() {
		
		itMatches("valid solution", {
			content: "ab(cd)ef",
		    pattern: "ab(cd)ef"
		});
		
		itMatches("valid solution for multiple groups", {
			content: "a(bc)d(ef)",
			pattern: "a(bc)d(ef)"
		});
		
		itDoesNotMatch("invalid solution", {
			content: "ab(cd)ef",
		    pattern: "abcd"
		});
		
		itMatches("group is found", {
			content: "ab(cd)ef",
		    pattern: "..(cd).."
		});

		itDoesNotMatch("group hasn't been found", {
			content: "ab(cd)ef",
			pattern: ".*"
		});
		
	});
	
	function itMatches(description, testCase) {
		it("matches if " + description, function() {
			// given:
			var example = new Example(testCase.content);
			
			// when:
			var result = example.match(testCase.pattern);
			
			// then:
			expect(result).toBeTruthy();
		});
	};
	function itDoesNotMatch(description, testCase) {
		it("does not match if " + description, function() {
			// given:
			var example = new Example(testCase.content);
			
			// when:
			var result = example.match(testCase.pattern);
			
			// then:
			expect(result).toBeFalsy();
		});
	};
});
